package a.a.cakekart;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import ui.MainActivity;

public class SignUpActivity extends AppCompatActivity {

    EditText username;
    EditText password;
    EditText email;
    EditText phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        username = (EditText) findViewById(R.id.signUpUsername);
        password = (EditText) findViewById(R.id.signUpPassword);
        email = (EditText) findViewById(R.id.signUpEmail);
        phone = (EditText) findViewById(R.id.signUpPhone);
        Button signup = (Button) findViewById(R.id.signUpBtn);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mUsername = username.getText().toString().trim();
                String mPassword = password.getText().toString().trim();
                String mEmail = email.getText().toString().trim();
                String mPhone = phone.getText().toString().trim();

                if(mUsername.isEmpty() || mPassword.isEmpty() || mEmail.isEmpty() || mPhone.isEmpty()){
                    LoginActivity.showAlertDialog(SignUpActivity.this, "Oops!!", "Some fields are empty.");
                }
                else {
                    ParseUser user = new ParseUser();
                    user.setUsername(mUsername);
                    user.setPassword(mPassword);
                    user.setEmail(mEmail);

                    user.put("MobileNo",mPhone);

                    user.signUpInBackground(new SignUpCallback() {
                        @Override
                        public void done(ParseException e) {
                            if(e == null){
                                startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                                finish();
                            }
                            else{
                                LoginActivity.showAlertDialog(SignUpActivity.this, "Try Again!!", "There is some problem to connecting with you account. Please try again.");
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
        finish();
    }
}
