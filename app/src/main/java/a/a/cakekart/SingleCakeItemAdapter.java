package a.a.cakekart;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by root on 11/6/15.
 */
public class SingleCakeItemAdapter extends ArrayAdapter<String> {

    int[] mImages;
    String[] mCakesName;
    Context mContext;

    public SingleCakeItemAdapter(Context context, int[] cakeImages, String[] cakesName){
        super(context, R.layout.single_cake_menu, cakesName);
        mContext = context;
        mImages = cakeImages;
        mCakesName = cakesName;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.single_cake_menu, null);
            holder = new ViewHolder();

            holder.cakeImage = (ImageView) convertView.findViewById(R.id.cakeImage);
            holder.cakeName = (TextView) convertView.findViewById(R.id.cakeName);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.cakeImage.setBackgroundResource(R.drawable.cake);
        holder.cakeName.setText(mCakesName[position]);

        return convertView;
    }

    public static class ViewHolder{
        ImageView cakeImage;
        TextView cakeName;
    }
}
