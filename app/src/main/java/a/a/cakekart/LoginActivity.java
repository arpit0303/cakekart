package a.a.cakekart;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import ui.MainActivity;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    EditText username;
    EditText password;
    TextView loginSignUpText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button loginButton = (Button) findViewById(R.id.loginBtn);
        username = (EditText) findViewById(R.id.loginUsername);
        password = (EditText) findViewById(R.id.loginPassword);
        loginSignUpText = (TextView) findViewById(R.id.loginSignupText);

        loginButton.setOnClickListener(this);
        loginSignUpText.setOnClickListener(this);
    }

    public static void showAlertDialog(Context context, String title, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title)
               .setMessage(msg)
                .setPositiveButton(android.R.string.ok, null);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.loginBtn:
                String mUsername = username.getText().toString().trim();
                String mPassword = password.getText().toString().trim();

                if(mUsername.isEmpty() || mPassword.isEmpty()){
                    showAlertDialog(LoginActivity.this, "Oops!!", "Some fields are empty.");
                }
                else {
                    ParseUser.logInInBackground(mUsername, mPassword, new LogInCallback() {
                        @Override
                        public void done(ParseUser parseUser, ParseException e) {
                            if(e == null){
                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                finish();
                            }
                            else{
                                showAlertDialog(LoginActivity.this, "Try Again!!", "There is some problem to connecting with you account. Please try again.");
                            }
                        }
                    });
                }
                break;
            case R.id.loginSignupText:
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
                finish();
                break;

            default:
                break;
        }
    }
}
