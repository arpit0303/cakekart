package ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import a.a.cakekart.R;

public class OrderSummaryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_summary);

        String cakeWeight = getIntent().getStringExtra("cakeWeight");
        String cakeFlavour = getIntent().getStringExtra("cakeFlavour");
        String cakePrice = getIntent().getStringExtra("cakePrice");
        String cakeTopping = getIntent().getStringExtra("cakeTopping");

        ParseObject order = new ParseObject("Order");
        order.put("cakeWeight", cakeWeight);
        order.put("cakeFlavour", cakeFlavour);
        order.put("cakePrice", cakePrice);
        order.put("cakeTopping", cakeTopping);
        order.saveInBackground();

        Button home = (Button) findViewById(R.id.home_btn);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OrderSummaryActivity.this, MainActivity.class));
                finish();
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(OrderSummaryActivity.this, MainActivity.class));
        finish();
    }
}
