package ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.parse.ParseQuery;
import com.parse.ParseUser;

import a.a.cakekart.LoginActivity;
import a.a.cakekart.R;
import a.a.cakekart.SingleCakeItemAdapter;


public class MainActivity extends ActionBarActivity {

    int[] cakeImages = {R.drawable.cake, R.drawable.cake, R.drawable.cake};
    String[] cakesName = {"a", "b", "c"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ParseUser.getCurrentUser() == null) {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        } else {
            android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.app_bar);
            setSupportActionBar(toolbar);

            GridView mCakeList = (GridView) findViewById(R.id.cakeList);

            SingleCakeItemAdapter adapter = new SingleCakeItemAdapter(MainActivity.this, cakeImages, cakesName);
            mCakeList.setAdapter(adapter);

            mCakeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(MainActivity.this, CakeDetailsActivity.class);
                    intent.putExtra("cakeType", position);
                    startActivity(intent);
                }
            });
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_location:
                break;
            case R.id.action_order:

                break;
            case R.id.action_logout:
                ParseUser.logOut();
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        System.exit(0);
    }
}
