package ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import a.a.cakekart.R;

public class ConfirmOrderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_order);

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView finalCakeWeight = (TextView) findViewById(R.id.finalCakeWeight);
        TextView finalCakeFlavour = (TextView) findViewById(R.id.finalCakeFlavour);
        TextView finalCakePrice = (TextView) findViewById(R.id.finalCakePrice);
        TextView finalCakeTopping = (TextView) findViewById(R.id.finalCakeTopping);

        finalCakeWeight.setText(getIntent().getStringExtra("cakeWeight"));
        finalCakeFlavour.setText(getIntent().getStringExtra("cakeFlavour"));
        finalCakePrice.setText(getIntent().getStringExtra("cakePrice"));
        finalCakeTopping.setText(getIntent().getStringExtra("cakeTopping"));

        Button cakeConfirm = (Button) findViewById(R.id.cakeConfirm);
        cakeConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfirmOrderActivity.this, OrderSummaryActivity.class);
                intent.putExtra("cakeWeight",getIntent().getStringExtra("cakeWeight"));
                intent.putExtra("cakeFlavour",getIntent().getStringExtra("cakeFlavour"));
                intent.putExtra("cakePrice",getIntent().getStringExtra("cakePrice"));
                intent.putExtra("cakeTopping",getIntent().getStringExtra("cakeTopping"));
                startActivity(intent);
                finish();
            }
        });

    }
}
