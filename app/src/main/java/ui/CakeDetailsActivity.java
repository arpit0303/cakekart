package ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import a.a.cakekart.R;

public class CakeDetailsActivity extends AppCompatActivity {

    Spinner cakeWeightSpinner;
    Spinner cakeFlavourSpinner;
    Spinner cakePriceSpinner;
    Spinner cakeToppingSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cake_details);

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        cakeWeightSpinner = (Spinner) findViewById(R.id.cakeWeight);
        cakeFlavourSpinner = (Spinner) findViewById(R.id.cakeFlavour);
        cakePriceSpinner = (Spinner) findViewById(R.id.cakePrice);
        cakeToppingSpinner = (Spinner) findViewById(R.id.cakeTopping);

        ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(CakeDetailsActivity.this,
                android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.cakeWeight));
        cakeWeightSpinner.setAdapter(weightAdapter);

        ArrayAdapter<String> flavourAdapter = new ArrayAdapter<String>(CakeDetailsActivity.this,
                android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.cakeFlavour));
        cakeFlavourSpinner.setAdapter(flavourAdapter);

        ArrayAdapter<String> priceAdapter = new ArrayAdapter<String>(CakeDetailsActivity.this,
                android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.cakePrice));
        cakePriceSpinner.setAdapter(priceAdapter);

        ArrayAdapter<String> toppingAdapter = new ArrayAdapter<String>(CakeDetailsActivity.this,
                android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.cakeTopping));
        cakeToppingSpinner.setAdapter(toppingAdapter);

        Button cakeSubmit = (Button) findViewById(R.id.cakeSubmit);

        cakeSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cakeWeightSpinner.getSelectedItemPosition() > 0 &&
                        cakeFlavourSpinner.getSelectedItemPosition() > 0 &&
                        cakePriceSpinner.getSelectedItemPosition() > 0 &&
                        cakeToppingSpinner.getSelectedItemPosition() > 0) {
                    Intent intent = new Intent(CakeDetailsActivity.this, ConfirmOrderActivity.class);
                    intent.putExtra("cakeWeight",cakeWeightSpinner.getSelectedItem().toString());
                    intent.putExtra("cakeFlavour",cakeFlavourSpinner.getSelectedItem().toString());
                    intent.putExtra("cakePrice",cakePriceSpinner.getSelectedItem().toString());
                    intent.putExtra("cakeTopping",cakeToppingSpinner.getSelectedItem().toString());
                    startActivity(intent);
                }
            }
        });
    }
}
