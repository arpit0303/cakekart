package fontable;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by root on 9/6/15.
 */
public class FontableEditText extends EditText {
    public FontableEditText(Context context) {
        super(context);
        init();
    }

    public FontableEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FontableEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init(){
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/LobsterTwo-Regular.otf");
        setTypeface(tf, 1);
    }
}
