package fontable;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by root on 9/6/15.
 */
public class FontableButton extends Button {
    public FontableButton(Context context) {
        super(context);
        init();
    }

    public FontableButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FontableButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init(){
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/LobsterTwo-Regular.otf");
        setTypeface(tf, 1);
    }
}
